package com.example.demo.entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
 
@Entity
@Table(name = "color")
/*@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id_color")*/
public class Color {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_color")
    private Integer idColor;
 
    private String nameColor;

    @JsonBackReference
    @ManyToMany(mappedBy = "colorOfDraw")
	private List<Draw> colorUtilisation;
    
    public Color(){}
    
	public Color(int i, String name) {
		// TODO Auto-generated constructor stub
		this.idColor= i ;
		this.nameColor = name;
	}

	public Integer getIdColor() {
		return idColor;
	}

	public void setIdColor(Integer id) {
		this.idColor = id;
	}

	public String getNameColor() {
		return nameColor;
	}

	public void setNameColor(String nameColor) {
		this.nameColor = nameColor;
	}

	@Override
	public String toString() {
		List<String> s = new ArrayList<String>();
		for(Draw d : colorUtilisation) {s.add(d.toStringForColor());}
		return "Color [id=" + idColor + ", nameColor=" + nameColor+"]";
	}

	public List<Draw> getColorUtilisation() {
		return colorUtilisation;
	}

	public void setColorUtilisation(List<Draw> colorUtilisation) {
		this.colorUtilisation = colorUtilisation;
	}

	
 
    
}