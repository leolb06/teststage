package com.example.demo.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "draw")
/*
 * @JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class,
 * property = "id_draw")
 */
public class Draw {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDraw;

	private String title;

	@JsonManagedReference
	@ManyToMany
	@JoinTable(name = "color_of_draw", joinColumns = @JoinColumn(name = "id_draw"), inverseJoinColumns = @javax.persistence.JoinColumn(name = "id_color"))
	private List<Color> colorOfDraw;
	// private List<Color> all_id_color;
	// private ColorController colorControler;

	public Draw() {
	}

	public Draw(int theId, String colors) {
		this.idDraw = theId;
		// this.all_id_color = getAllId(colors);
		// colorControler = new ColorController();

	}

	/*
	 * private List<Color> getAllId(String colors) { String[] allId =
	 * colors.split(":"); List<Color> listColor = new ArrayList<Color>();
	 * 
	 * for (String id : allId) {
	 * listColor.add(colorControler.getOne(Integer.parseInt(id))); } return
	 * listColor; }
	 */

	public Integer getIdDraw() {
		return idDraw;
	}

	public void setIdDraw(Integer id_draw) {
		this.idDraw = id_draw;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Color> getColorOfDraw() {
		return colorOfDraw;
	}

	public void setColorOfDraw(List<Color> colorOfDraw) {
		this.colorOfDraw = colorOfDraw;
	}

	public String toStringForColor() {
		return "Draw [id_draw=" + idDraw + ", title=" + title + ", colorOfDraw=" + colorOfDraw + "]";
	}

}
