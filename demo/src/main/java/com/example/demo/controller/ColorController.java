package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.entity.Color;
import com.example.demo.entity.Draw;
import com.example.demo.repository.ColorRepository;

@RestController
public class ColorController {
	@Autowired
	private ColorRepository colorRepo;

	// Return all color of the BDD
	@GetMapping("/color")
	public List<Color> listAll() {
		List<Color> listColor = colorRepo.findAll();
		return listColor;
	}

	// Return the color associate to the url id
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@GetMapping("/color/{id}")
	public Color getOne(@PathVariable int id) {
		Optional<Color> search = colorRepo.findById(id);
		Color c = search.orElse(null);
		if (c == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
		return c;
	}

	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@GetMapping("/colorUtilisation/{id}")
	public List<Draw> getOneColorUsage(@PathVariable int id) {
		Optional<Color> search = colorRepo.findById(id);
		Color c = search.orElse(null);
		if (c == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");

		List<Draw> allDraw = c.getColorUtilisation();
		List<String> allNameDraw = new ArrayList<String>();

		for (Draw d : allDraw) {
			allNameDraw.add(d.getTitle());
		}
		// return allNameDraw;
		return allDraw;
	}


	@PostMapping("/color")
	public Color postColor(@RequestBody Color newColor) {
		newColor.setIdColor(null);
		newColor = colorRepo.saveAndFlush(newColor);
		return newColor;
	}

	// Update the color at the url id
	@PutMapping("/color/{id}")
	public Color put(@RequestBody Color newColor, @PathVariable int id) {
		Optional<Color> currentColor = this.colorRepo.findById(id);
		Color c = currentColor.orElse(null);
		if (c == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
		newColor.setIdColor(id);
		newColor = colorRepo.saveAndFlush(newColor);

		return newColor;
	}

	// Delete the color at the url id
	@DeleteMapping("/color/{id}")
	public boolean delete(@PathVariable int id) {
		// checker si l'objet existe d�j�
		Optional<Color> currentColor = this.colorRepo.findById(id);
		Color c = currentColor.orElse(null);
		if (c == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
		} else {
			this.colorRepo.deleteById(id);
			return true;
		}

		// return "Color n�"+id+" has been deleted";
	}

}
