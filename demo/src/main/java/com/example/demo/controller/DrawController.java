package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.entity.Draw;
import com.example.demo.repository.DrawRepository;

@RestController
public class DrawController {

	@Autowired
	private DrawRepository drawRepo;

	// Return all color of the BDD
	@GetMapping("/draw")
	public List<Draw> listAll() {
		List<Draw> listColor = drawRepo.findAll();
		return listColor;
	}

	// Return the color associate to the url id
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@GetMapping("/draw/{id}")
	public Draw getOne(@PathVariable int id) {
		Optional<Draw> search = drawRepo.findById(id);
		Draw d = search.orElse(null);
		if (d == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
		return d;
	}

	/*
	 * @ResponseStatus(value = HttpStatus.NOT_FOUND)
	 * 
	 * @GetMapping("/colorUtilisation/{id}") public Draw
	 * getOneColorUsage(@PathVariable int id) { Optional<Draw> search =
	 * drawRepo.findById(id); Draw d = search.orElse(null); if (d == null) throw new
	 * ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
	 * 
	 * //List<Draw> allDraw = d.getColorUtilisation(); //List<String> allNameDraw =
	 * new ArrayList<String>();
	 * 
	 * /*for(Draw d : allDraw) { allNameDraw.add(d.getTitle()); } //return
	 * allNameDraw; return d;
	 * 
	 * 
	 * }
	 */

	// add a Color to the BDD
	/*
	 * @PostMapping("/color") public Color postColor(@RequestBody Color newColor) {
	 * colorRepo.save(newColor); return newColor; }
	 */

	@PostMapping("/draw")
	public Draw postColor(@RequestBody Draw newDraw) {
		newDraw.setIdDraw(null);
		newDraw = drawRepo.saveAndFlush(newDraw);
		return newDraw;
	}

	// Update the color at the url id
	@PutMapping("/draw/{id}")
	public Draw put(@RequestBody Draw newDraw, @PathVariable int id) {
		Optional<Draw> currentDraw = this.drawRepo.findById(id);
		Draw c = currentDraw.orElse(null);
		if (c == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
		newDraw.setIdDraw(id);
		newDraw = drawRepo.saveAndFlush(newDraw);

		return newDraw;
	}

	// Delete the color at the url id
	@DeleteMapping("/draw/{id}")
	public boolean delete(@PathVariable int id) {
		// checker si l'objet existe d�j�
		Optional<Draw> currentDraw = this.drawRepo.findById(id);
		Draw d = currentDraw.orElse(null);
		if (d == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
		} else {
			this.drawRepo.deleteById(id);
			return true;
		}

		// return "Color n�"+id+" has been deleted";
	}

}
