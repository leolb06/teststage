package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Draw;

public interface DrawRepository extends JpaRepository<Draw, Integer> {
}
