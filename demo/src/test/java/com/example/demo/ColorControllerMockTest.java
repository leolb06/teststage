package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.controller.ColorController;
import com.example.demo.entity.Color;
import com.example.demo.entity.Draw;
import com.example.demo.repository.ColorRepository;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
public class ColorControllerMockTest {

	@InjectMocks
	private ColorController controller;
	@Spy
	private ColorRepository myRepo;

	@Test
	public void getAll() {
		List<Color> list = new ArrayList<Color>();
		list.add(new Color(0, "Red"));
		list.add(new Color(1, "Red"));
		list.add(new Color(2, "Red"));
		when(myRepo.findAll()).thenReturn(list);
		List<Color> result = controller.listAll();
		int sizeList = result.size();
		assertEquals((int) 3, sizeList);
	}

	@Test
	public void getOne() {
		Color color = new Color(2, "Red");
		Optional<Color> optionColor = Optional.of(color);
		when(myRepo.findById(2)).thenReturn(optionColor);
		Color result = controller.getOne(2);
		assertTrue(color.equals(result));
	}

	@Test
	public void getOne404() {
		Color color = new Color(2, "Red");
		Optional<Color> optionEmpty = Optional.empty();
		when(myRepo.findById(2)).thenReturn(optionEmpty);
		try {
			Color result = controller.getOne(2);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");

	}

	@Test
	public void postOne() {
		Color color2 = new Color(12, "Red");
		Color colorAfterPost = new Color(12, "Red");
		when(myRepo.saveAndFlush(color2)).thenReturn(colorAfterPost);
		Color result = controller.postColor(color2);
		assertTrue(result.equals(colorAfterPost));
	}

	@Test
	public void postOneWithFalseId() {
		Color color2 = new Color(2, "Red");
		Color colorAfterPost = new Color(5, "Red");
		when(myRepo.saveAndFlush(color2)).thenReturn(colorAfterPost);
		Color result = controller.postColor(color2);
		assertTrue(result.equals(colorAfterPost));
	}

	@Test
	public void putExist() {
		Color newColor = new Color(20, "Blue");
		Color findColor = new Color(2, "Red");
		Color trueColor = new Color(2, "Blue");

		Optional<Color> optionColor = Optional.of(findColor);

		when(myRepo.findById(2)).thenReturn(optionColor);
		when(myRepo.saveAndFlush(newColor)).thenReturn(trueColor);

		Color result = controller.put(newColor, 2);
		System.out.println(newColor.getNameColor());
		System.out.println(result.getNameColor());

		assertEquals(result.getNameColor(), newColor.getNameColor());
	}

	@Test
	public void putNoExist() {
		Color newColor = new Color(20, "Blue");
		Optional<Color> optionColor = Optional.empty();

		when(myRepo.findById(2)).thenReturn(optionColor);

		try {
			Color result = controller.put(newColor, 12);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");
	}

	@Test
	public void deleteFalse() {
		Optional<Color> optionColor = Optional.empty();
		when(myRepo.findById(12)).thenReturn(optionColor);
		try {
			boolean result = controller.delete(12);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");
	}

	@Test
	public void deleteTrue() {
		Color findColor = new Color(2, "Blue");
		Optional<Color> optionColor = Optional.of(findColor);
		Optional<Color> emptyOption = Optional.empty();

		when(myRepo.findById(2)).thenReturn(optionColor);
		controller.delete(2);
		when(myRepo.findById(2)).thenReturn(emptyOption);
		Optional<Color> result = myRepo.findById(2);
		assertTrue(result.isEmpty());
	}
	
	

	@Test
	public void getColorUsage() {
		Color findColor = new Color(2, "Blue");
		List<Draw> listUsage = new ArrayList<Draw>();
		Draw draw0 = new Draw(0,"food");
		Draw draw1 = new Draw(1,"tree");
		listUsage.add(draw0);
		listUsage.add(draw1);
		findColor.setColorUtilisation(listUsage);
		Optional<Color> optionColor = Optional.of(findColor);

		when(myRepo.findById(2)).thenReturn(optionColor);
		List<Draw> result = controller.getOneColorUsage(2);

		assertEquals(result, listUsage);
	} 
	
	@Test
	public void getFalseColorUsage() {
		Color findColor = new Color(2, "Blue");
		List<Draw> listUsage = new ArrayList<Draw>();
		findColor.setColorUtilisation(listUsage);
		Optional<Color> optionColor = Optional.of(findColor);

		when(myRepo.findById(2)).thenReturn(optionColor);
		try {
			List<Draw> result = controller.getOneColorUsage(3);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");
	}
}

