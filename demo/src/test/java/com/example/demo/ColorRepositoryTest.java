package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.controller.ColorController;
import com.example.demo.entity.Color;

//@ExtendWith(MockitoExtension.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ColorRepositoryTest {

	@Autowired
	private ColorController cont;

	@Test
	public void getAll() {
		List<Color> list = cont.listAll();
		assertEquals(3, list.size());
	}

	@Test 
	public void getOne() {
		Color red = cont.getOne(1);
		assertTrue("red".equalsIgnoreCase(red.getNameColor()));
	}

	@Test
	public void getFalseId() {
		try {
			cont.getOne(100);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");
	}

	@Test
	public void createRow() {
		Color color = new Color(12, "orange");
		Color c = cont.postColor(color);
		assertEquals(color.getNameColor(), c.getNameColor());
	}

	@Test
	public void createNopeRow() {
		Color color = new Color(1, "Blue lagon");
		int oldId = color.getIdColor();
		Color c = cont.postColor(color);
		assertNotEquals(c.getIdColor(), oldId); // Attention c'est equals pour comparer des objets
	}

	@Test
	public void putExist() {
		Color color = new Color(12, "Pink");
		Color c = cont.put(color, 1);
		assertEquals(color.getNameColor(), c.getNameColor());
	}

	@Test
	public void putNoExist() {
		Color color = new Color(12, "Tomato");
		try {
			cont.put(color, 21);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");
	}

	@Test
	public void supprExist() {
		boolean res = cont.delete(1);
		assertTrue(res);
	}

	@Test
	public void supprNoExist() {
		try {
			cont.delete(1);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
	}


}