package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.controller.ColorController;
import com.example.demo.controller.DrawController;
import com.example.demo.entity.Color;
import com.example.demo.entity.Draw;
import com.example.demo.repository.ColorRepository;
import com.example.demo.repository.DrawRepository;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
public class DrawControllerMockTest {

	@InjectMocks
	private DrawController controller;
	@Spy
	private DrawRepository myRepo;

	@Test
	public void getAll() {
		List<Draw> list = new ArrayList<Draw>();
		list.add(new Draw(0, "sheep"));
		list.add(new Draw(1, "plane"));
		list.add(new Draw(2, "moon"));
		when(myRepo.findAll()).thenReturn(list);
		List<Draw> result = controller.listAll();
		int sizeList = result.size();
		assertEquals((int) 3, sizeList);
	}

	@Test
	public void getOne() {
		Draw draw = new Draw(2, "moon");
		Optional<Draw> optionColor = Optional.of(draw);
		when(myRepo.findById(2)).thenReturn(optionColor);
		Draw result = controller.getOne(2);
		assertTrue(draw.equals(result));
	}

	@Test
	public void getOne404() {
		Optional<Draw> optionEmpty = Optional.empty();
		when(myRepo.findById(2)).thenReturn(optionEmpty);
		try {
			controller.getOne(2);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");

	}

	@Test
	public void postOne() {
		Draw draw2 = new Draw(12, "earth");
		Draw drawAfterPost = new Draw(12, "earth");
		when(myRepo.saveAndFlush(draw2)).thenReturn(drawAfterPost);
		Draw result = controller.postColor(draw2);
		assertTrue(result.equals(drawAfterPost));
	}

	@Test
	public void postOneWithFalseId() {
		Draw draw2 = new Draw(2, "moon");
		Draw drawAfterPost = new Draw(5, "moon");
		when(myRepo.saveAndFlush(draw2)).thenReturn(drawAfterPost);
		Draw result = controller.postColor(draw2);
		assertTrue(result.equals(drawAfterPost));
	}

	@Test
	public void putExist() {
		Draw newDraw = new Draw(20, "moon");
		Draw findDraw = new Draw(2, "sheep");
		Draw trueDraw = new Draw(2, "plane");

		Optional<Draw> optionDraw = Optional.of(findDraw);

		when(myRepo.findById(2)).thenReturn(optionDraw);
		when(myRepo.saveAndFlush(newDraw)).thenReturn(trueDraw);

		Draw result = controller.put(newDraw, 2);

		assertEquals(result.getTitle(), newDraw.getTitle());
	}

	@Test
	public void putNoExist() {
		Draw newDraw = new Draw(20, "Blue");
		Optional<Draw> optionDraw = Optional.empty();

		when(myRepo.findById(2)).thenReturn(optionDraw);

		try {
			Draw result = controller.put(newDraw, 12);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");
	}

	@Test
	public void deleteFalse() {
		Optional<Draw> optionDraw = Optional.empty();
		when(myRepo.findById(12)).thenReturn(optionDraw);
		try {
			boolean result = controller.delete(12);
		} catch (ResponseStatusException e) {
			assertTrue(e.getMessage().contains("404"));
			return;
		}
		fail("Aurait d� renvoyer une ResponseStatusException");
	}

	@Test
	public void deleteTrue() {
		Draw findDraw = new Draw(2, "moon");
		Optional<Draw> optionDraw = Optional.of(findDraw);
		Optional<Draw> emptyOption = Optional.empty();

		when(myRepo.findById(2)).thenReturn(optionDraw);
		controller.delete(2);
		when(myRepo.findById(2)).thenReturn(emptyOption);
		Optional<Draw> result = myRepo.findById(2);
		assertTrue(result.isEmpty());
	}
	
	
}

